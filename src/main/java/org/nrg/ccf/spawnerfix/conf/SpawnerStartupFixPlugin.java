package org.nrg.ccf.spawnerfix.conf;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "spawnerStartupFixPlugin",
			name = "Spawner Startup Fix Plugin"
			//log4jPropertiesFile = "/META-INF/resources/structuralQcLog4j.properties"
		)
@ComponentScan({ 
		"org.nrg.ccf.spawnerfix.conf",
		"org.nrg.ccf.spawnerfix.components",
		"org.nrg.ccf.spawnerfix.xapi"
	})
@Slf4j
public class SpawnerStartupFixPlugin {
	
	public SpawnerStartupFixPlugin() {
		log.info("Configuring the Spawner Startup Fix Plugin.");
	}
	
}
