package org.nrg.ccf.spawnerfix.xapi;

import org.nrg.ccf.spawnerfix.components.SpawnerInitializer;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@Lazy
@XapiRestController
@Api(description = "Spawner Initialization API")
public class SpawnerInitializationApi extends AbstractXapiRestController {
	
	private SpawnerInitializer _initializer;

	@Autowired
	@Lazy
	public SpawnerInitializationApi(UserManagementServiceI userManagementService, RoleHolder roleHolder,
			SpawnerInitializer initializer) {
		super(userManagementService, roleHolder);
		_initializer = initializer;
	}
	
    @ApiOperation(value = "Re-initialize spawner elements",
                  response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Spawner elements successfully re-initialized."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = {"/spawnerInitialization/reinitialize"}, 
    		produces = MediaType.TEXT_PLAIN_VALUE, restrictTo=AccessLevel.Admin, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> reinitialize() throws Exception {
    	_initializer.initializeSpawner();
    	return new ResponseEntity<>(HttpStatus.OK);
    }
	
}
