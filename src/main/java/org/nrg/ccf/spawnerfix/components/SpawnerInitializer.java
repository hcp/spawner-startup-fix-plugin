package org.nrg.ccf.spawnerfix.components;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.nrg.framework.task.XnatTask;
import org.nrg.framework.task.services.XnatTaskService;
import org.nrg.framework.utilities.NameUtils;
import org.nrg.xnat.spawner.entities.SpawnerElement;
import org.nrg.xnat.spawner.exceptions.InvalidElementIdException;
import org.nrg.xnat.spawner.preferences.SpawnerPreferences;
import org.nrg.xnat.spawner.services.SpawnerResourceLocator;
import org.nrg.xnat.spawner.services.SpawnerService;
import org.nrg.xnat.task.AbstractXnatTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;

import com.google.common.base.Joiner;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@XnatTask(taskId = "SpawnerInitializer", description = "Spawner Initialization Task", 
	defaultExecutionResolver = "SingleNodeExecutionResolver", executionResolverConfigurable = true)
public class SpawnerInitializer extends AbstractXnatTask implements Runnable {
	
    Pattern REGEX_NEW_ELEMENT               = Pattern.compile("^([A-z0-9_-]+):[\\s]*$");
    Pattern REGEX_COMMENT                   = Pattern.compile("^[\\s]*#.*$");
    Pattern REGEX_RESOURCE_NAMESPACE        = Pattern.compile("^.*META-INF/xnat/spawner/(?<nselement>.+?)(?<suffix>-elements)?.yaml$");
    String  DEFAULT_SPAWNER_LOCATOR_PATTERN = "classpath*:META-INF/xnat/spawner/**/*.yaml";
	
	private SpawnerPreferences _spawnerPreferences;
	private SpawnerService _spawnerService;
	private List<SpawnerResourceLocator> _resourceLocators;
	
	@PostConstruct
	public void initialize() {
		try {
			if (_spawnerPreferences.getPurgeAndRefreshOnStartup()) {
				log.info("Initializing SpawnerInitializer - set spawner purge_and_refresh_on_startup preference setting to FALSE.");
				_spawnerPreferences.setPurgeAndRefreshOnStartup(false);
			}
			run();
		} catch (Exception e) {
			log.error("ERROR:  Exception thrown initializing spawner",e);
		}
	}

	@Autowired
	public SpawnerInitializer(XnatTaskService _taskService, SpawnerPreferences spawnerPreferences, SpawnerService spawnerService,
			 final List<SpawnerResourceLocator> resourceLocators) {
		super(_taskService);
		_resourceLocators = resourceLocators;
		_spawnerPreferences = spawnerPreferences;
		_spawnerService = spawnerService;
	}

	@Override
	public void run() {
		log.debug("Run SpawnerInitialization");
		if (!shouldRunTask()) {
			log.debug("Spawner initialization is not configured to run on this node.  Skipping.");
			return;
		}
		try {
			initializeSpawner();
		} catch (Exception e) {
			log.error("ERROR:  Exception thrown initializing spawner",e);
		}
	}

	public void initializeSpawner() throws InvalidElementIdException {
		
        log.debug("Spawner preferences indicates spawner namespaces should be purged and reloaded.");
        for (final String namespace : _spawnerService.getNamespaces()) {
            log.debug("Purging namespace {}", namespace);
            _spawnerService.deleteNamespace(namespace);
        }

        final List<SpawnerElement> elements = getSpawnerElementDefinitions();
        if (elements.size() > 0) {
            log.debug("Preparing to process {} spawner element definitions to look for new spawner elements.", elements.size());
            for (final SpawnerElement element : elements) {
                if (!_spawnerService.getNamespacedElementIds(element.getNamespace()).contains(element.getElementId())) {
                    if (log.isDebugEnabled()) {
                        log.debug("Creating a new spawner element with element ID " + element.getElementId());
                    }
                    _spawnerService.create(element);
                }
            }
        }
		
	}
	
	/*
	 * Private methods copied from HibernateSpawnerService class
	 */
	
    private List<SpawnerElement> getSpawnerElementDefinitions() throws InvalidElementIdException {
        final List<SpawnerElement> elements = new ArrayList<>();
        try {
            for (final SpawnerResourceLocator locator : _resourceLocators) {
                for (final Resource resource : locator.getResources()) {
                    elements.addAll(importElementsFromResource(resource));
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("An error occurred trying to locate XNAT spawner element definitions.", e);
        }

        return elements;
    }
    
    private List<SpawnerElement> importElementsFromResource(final Resource resource) throws IOException, InvalidElementIdException {
        final String namespace = getNamespaceFromResourceName(getResourcePath(resource));
        final List<SpawnerElement> elements = new ArrayList<>();
        try (final InputStream input = resource.getInputStream(); final BufferedReader reader = new BufferedReader(new InputStreamReader(input))) {
            String line;
            StringBuilder incoming = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                final Matcher newElement = REGEX_NEW_ELEMENT.matcher(line);
                if (newElement.find()) {
                    if (incoming.length() > 0) {
                        addParsedElement(resource, namespace, elements, incoming);
                    }
                    incoming = new StringBuilder(line).append(System.lineSeparator());
                } else {
                    final Matcher comment = REGEX_COMMENT.matcher(line);
                    if (comment.matches()) {
                        if (log.isDebugEnabled()) {
                            log.debug("Ignoring line from incoming YAML because it's a comment: " + line);
                        }
                    } else {
                        incoming.append(line).append(System.lineSeparator());
                    }
                }
            }
            if (incoming.length() > 0) {
                addParsedElement(resource, namespace, elements, incoming);
            }
        }
        return elements;
    }
    
    private String getNamespaceFromResourceName(final String resource) {
        final Matcher matcher = REGEX_RESOURCE_NAMESPACE.matcher(resource);
        if (matcher.find()) {
            return Joiner.on(":").join(NameUtils.convertResourceNamesToBeanIds(Arrays.asList(matcher.group("nselement").split("[/.]"))));
        }
        return null;
    }
    
    private String getResourcePath(final Resource resource) {
        if (resource instanceof UrlResource) {
            try {
                return resource.getURI().toString();
            } catch (IOException e) {
                log.warn("Got an error trying to get the URI of a UrlResource, using toString: " + resource.toString());
                return resource.toString();
            }
        }
        if (resource instanceof FileSystemResource) {
            return ((FileSystemResource) resource).getPath();
        }
        if (resource instanceof ClassPathResource) {
            return ((ClassPathResource) resource).getPath();
        }
        if (resource instanceof InputStreamResource) {
            try {
                return resource.getURI().toString();
            } catch (IOException e) {
                log.warn("Got an error trying to get the URI of an InputStreamResource, using toString: " + resource.toString());
                return resource.toString();
            }
        }
        log.info("No standard way to return a path from a resource of type " + resource.getClass() + ", using toString: " + resource.toString());
        return resource.toString();
    }

    private void addParsedElement(final Resource resource, final String namespace, final List<SpawnerElement> elements, final StringBuilder incoming) throws InvalidElementIdException, IOException {
        final SpawnerElement element = _spawnerService.parse(incoming.toString());
        if (StringUtils.isBlank(element.getElementId())) {
            log.warn("Ignoring possibly valid YAML in the resource located at " + resource.getURI() + ": no element ID was specified before encountering indented text:" + System.lineSeparator() + element.getYaml());
        } else {
            element.setNamespace(namespace);
            elements.add(element);
        }
    }

}
