# README #

*spawner-startup-fix-plugin*

Fixes initialization issue in a multi-node environment where multiple machines are trying to update the spawner elements table concurrently and failing.
